import './App.css';
import Month from './Month';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Month />
      </header>
    </div>
  );
}

export default App;

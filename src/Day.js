import React from "react";

const Day = ({month}) => {
    const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    const day = new Date(`2021 ${month} 1`).getDay();
    let date = weekday[day]
    return (
        <div>
            Month {month}/2021 starts {date}
        </div>
    )
}

export default Day;
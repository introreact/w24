import React from "react";
import Day from "./Day";
import { useState } from "react";

const Month = () => {
    const [month, setMonth] = useState('January');
    let months = [
        {id:1, name: "January"},
        {id:2, name: "February"},
        {id:3, name: "March"},
        {id:4, name: "April"},
        {id:5, name: "May"},
        {id:6, name: "June"},
        {id:7, name: "July"},
        {id:8, name: "August"},
        {id:9, name: "September"},
        {id:10, name: "October"},
        {id:11, name: "November"},
        {id:12, name: "December"}
    ]

    var monthList = months.map(month => {
        return (
        <option key={month.id} value={month.name}>
            {month.name}
        </option>)
    })
    return (
        <div>
            <select onChange={({target}) => setMonth(target.value)}>
                {monthList}
            </select>
            <Day month={month} />
        </div>
    )
}

export default Month;